angular.module("persist").controller("editorController", ["$scope", "$timeout", function ($scope, $timeout) {
	$scope.editorData = "";

	$scope.sendEvent = function () {
		console.log("Send Event");
		$timeout(function () {
			$scope.$broadcast('template-editor:body:insert', "Event");
		}, 0);
	}
}]);