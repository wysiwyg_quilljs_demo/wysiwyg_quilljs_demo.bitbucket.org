
angular.module('persist')
.directive('ckeditor', ['$timeout', 'CKE', function ($timeout, CKE) {

  function linkFn($scope, el, attr, ngModel) {

    // local skin with cdn:
    // http://cdn.ckeditor.com/
    var height = '300px';
    if( attr.height ) { height = attr.height; }

    var ck = CKE.replace(el[0], {
      // extraPlugins: 'placeholder',
      skin: 'moono',
      // in order to remove context menu, must also remove liststyle and tabletools,
      // contextmenu is a dependency for tehse two
      // removePlugins: 'elementspath,contextmenu,liststyle,tabletools',  // remove bottom bar
      // plugin elementspath is the bottom status bar
      // plugin magicline is the annoying that shows up on hover
      removePlugins: 'elementspath,magicline',

      // enable native browser spell checker
      // disableNativeSpellChecker: false,

      // sets default dropdown option, actual styling is set elsewhere
      font_defaultLabel : 'Arial',
      fontSize_defaultLabel : '13px',

      resize_enabled: ($scope.resizable === true),
      enterMode : CKE.ENTER_BR,

      // strip formatting on paste
      forcePasteAsPlainText: ($scope.pasteFormat !== true),

      // prevent CK from escaping double quote into &quote; and single quote into &#39;
      // this is required to enable liquid template's default value feature
      entities: false,

      toolbar: [
        { name: 'basicstyles', items : [ 'Bold','Italic', '-', 'RemoveFormat'] },
        { name: 'styles', items : [ 'FontSize' ] },
        { name: 'paragraph', items : ['BulletedList' ] },
        { name: 'links',  items : [ 'Link','Unlink'] },
        { name: 'tools',  items : [ 'Maximize'] },
        { name: 'src',    items : [ 'Source'] }
      ],
      allowedContent: true,
      height: height,
    });

    // insert variable into template body
    $scope.$on('template-editor:body:insert', function(ev, content) {
      ck.insertHtml( content );
    });

    function updateModel() {
      $timeout(function() {
        $scope.$apply(function() {
          ngModel.$setViewValue(ck.getData());
        });
      },50);
    }

    ck.on('instanceReady', function() {
      ck.setData(ngModel.$viewValue);
    });

    ck.on('change', updateModel);
    ck.on('key', updateModel);

    ck.on('focus', function() {
      $scope.$parent.$apply(attr.ngFocus);
    });

    ngModel.$render = function(value) {
      ck.setData(ngModel.$viewValue);
    };
  }

  return {
    require: 'ngModel',
    link: linkFn,
    scope: {
      pasteFormat : '=',
      resizable: '=',
    }
  };
}]);