var express = require("express");
 
const PORT = 5000;
const staticFilesPath = "web";

var app = express();
app.use(express.static(staticFilesPath));

console.log("Listen to port " + PORT);
app.listen(PORT);

